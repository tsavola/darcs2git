#!/usr/bin/env python

import sys
from darcs2git import main

if len(sys.argv) not in (3, 4):
	print >>sys.stderr, 'Usage: %s SOURCE TARGET [AUTHORS]' % sys.argv[0]
	sys.exit(1)

authorfile = None
if len(sys.argv) >= 4:
	authorfile = sys.argv[3]

main.main(sys.argv[1], sys.argv[2], authorfile)

# Copyright 2006-2007  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import gzip
import os
import popen2
import re
import shutil
import sys
import tempfile
from xml.dom.minidom import parse as parse_xml

darcs_command = os.environ.get('DARCS', 'darcs')
git_command = os.environ.get('GIT', 'git')

def run(args):
	print 'Execute:', ' '.join(args)
	status = os.spawnvp(os.P_WAIT, args[0], args)
	if status != 0:
		raise Exception('%s failed' % args[0])

def darcs(*args):
	os.rename('.git', '../git')
	run([darcs_command] + list(args))
	os.rename('../git', '.git')

def git(*args):
	if git_command.endswith('-'):
		run([git_command + args[0]] + list(args[1:]))
	else:
		run([git_command] + list(args))

def op_addfile(name):
	if not os.path.exists(name):
		print 'Creating empty file:', name
		open(name, 'w').close()

	git('update-index', '--add', name)

def op_rmfile(name):
	if os.path.exists(name):
		print 'Deleting file:', name
		os.remove(name)

	git('update-index', '--remove', name)

def op_move(source, target):
	if os.path.exists(source):
		tempfile = os.path.join('..', os.path.basename(source))
		os.rename(source, tempfile)
	else:
		tempfile = None

	os.rename(target, source)
	git('mv', source, target)

	if tempfile:
		os.rename(tempfile, source)

def op_modify(name):
	if os.path.exists(name):
		git('update-index', name)

operations = (
	(re.compile(r'^addfile ([^ ]+)$'), op_addfile),
	(re.compile(r'^rmfile ([^ ]+)$'), op_rmfile),
	(re.compile(r'^move ([^ ]+) ([^ ]+)$'), op_move),
	(re.compile(r'^hunk ([^ ]+) .+$'), op_modify),
	(re.compile(r'^replace ([^ ]+) .+$'), op_modify),
)

def find_delimiter(text, char):
	skip = False

	for i in xrange(len(text)):
		c = text[i]

		if skip:
			skip = False
			continue

		if c == '\\':
			skip = True
			continue

		if c == char:
			return i

	return -1

def perform_operations(repodir, hash):
	path = os.path.join(repodir, '_darcs', 'patches', hash)
	if not os.path.exists(path):
		raise Exception('%s does not exist' % path)

	file = gzip.GzipFile(path, 'r')

	lines = []
	header = True

	for line in file:
		if header:
			i = find_delimiter(line, ']')
			if i < 0:
				continue

			line = line[i+1:]
			if line[0] == ' ':
				line = line[1:]

			header = False

		lines.append(line)

	file.close()

	done = False

	for line in lines:
		for regex, func in operations:
			match = regex.match(line)
			if match:
				args = []
				for name in match.groups():
					if name.startswith('./'):
						name = name[2:]
					if name.endswith('\n'):
						name = name[:-1]
					args.append(name)

				func(*args)
				done = True
				break

	return done

def main(sourcedir, targetdir, authorfile):
	if os.path.exists(targetdir):
		raise Exception('%s already exists' % targetdir)

	authormap = {}
	if authorfile:
		f = open(authorfile)
		for line in f.readlines():
			key, value = line.split('\t', 1)
			authormap[key.strip()] = value.strip()
		f.close()

	tempdir = tempfile.mkdtemp()
	workdir = os.path.join(tempdir, 'work')
	print 'Working in', workdir
	os.mkdir(workdir)
	os.chdir(workdir)

	git('init-db')
	darcs('init')

	command = [darcs_command, 'changes', '--xml-output', '--reverse', \
	           '--repodir=' + sourcedir]
	print 'Execute:', ' '.join(command)
	process = popen2.Popen3(command)
	document = parse_xml(process.fromchild)
	status = process.wait()
	if status != 0:
		raise Exception('%s failed' % darcs_command)

	for element in document.documentElement.getElementsByTagName('patch'):
		author = element.getAttribute('author')
		if '<' not in author or '@' not in author:
			if author not in authormap:
				print >>sys.stderr, 'Warning: "%s" not listed in "%s"' \
				    % (author, authorfile)
			author = authormap[author]
		date = element.getAttribute('date')
		local_date = element.getAttribute('local_date')
		inverted = element.getAttribute('inverted')
		hash = element.getAttribute('hash')

		name_list = element.getElementsByTagName('name')
		if name_list:
			name = name_list[0].childNodes[0].nodeValue
		else:
			raise Exception('Patch has no name')

		if inverted.lower() == 'true':
			name = 'UNDO: ' + name

		comment_list = element.getElementsByTagName('comment')
		if comment_list:
			comment = comment_list[0].childNodes[0].nodeValue
		else:
			comment = None

		print
		print 'Patch:', name

		darcs('pull', '--matches', 'hash ' + hash, '-a', sourcedir)

		ops = perform_operations(sourcedir, hash)

		if comment:
			message = name + '\n' + comment
		else:
			message = name

		if name.startswith('TAG '):
			tagname = name[4:].strip()
			i = tagname.find(' **')
			if i >= 0:
				tagname = tagname[:i]
			git('tag', tagname)
		elif ops:
			os.environ['GIT_AUTHOR_DATE'] = local_date
			git('commit', '--author', author, '-m', message)
			del os.environ['GIT_AUTHOR_DATE']
		else:
			print 'Nothing to commit'

	print

	gitdir = os.path.join(workdir, '.git')
	print 'Renaming', gitdir, 'as', targetdir
	shutil.move(gitdir, targetdir)

	print 'Removing', tempdir
	os.chdir('/')
	run(['rm', '-r', tempdir])
